@extends('app')

@section ('content')

        <h1>{{$book->title}}</h1>


    <article>
        {{$book->author}}
    </article>

    @stop
    --

{{--
@extends('app')
@section('content')
    <h1>Book Show</h1>

    <form class="form-horizontal">

        <div class="form-group">
            <label for="book_id" class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="book_id" placeholder={{$book->book_id}} readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="title" placeholder={{$book->title}} readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="author" class="col-sm-2 control-label">Author</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="author" placeholder={{$book->author}} readonly>
            </div>
        </div>
               <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="{{ url('books')}}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </form>
@stop

--}}