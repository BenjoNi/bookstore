@extends ('app')
{{--
<a href="{{url('/books/create')}}" class="btn btn-success">Create Book</a>
<hr>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr class="bg-info">
        <th>Id</th>
        <th>Title</th>
        <th>Author</th>
        <th>Published:</th>
        <th colspan="3">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($books as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td>{{ $book->title }}</td>
            <td>{{ $book->author }}</td>
            <td>{{ $book->published_at }}</td>

            <td><a href="{{url('books',$book->id)}}" class="btn btn-primary">Read</a></td>
            <td><a href="{{route('books.edit',$book->id)}}" class="btn btn-warning">Update</a></td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route'=>['books.destroy', $book->id]]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach

    </tbody>


</table>
@endsection
--}}



@section('content')



    <h1> Available books:</h1>
    <a href="books/create"><button type="button">Create Book</button></a>


    @foreach($books as $book)
        <article>

            <h2>
                {{$book->title}} <div style="position: absolute;left: 150px;:"></div>
                <a href="edit"><button type="button">Edit Book</button></a>
                <a href="books"><button type="button">Delete Book</button></a>
            </h2>



            <div class="body">{{$book->author}}</div>
        </article>

    @endforeach
@endsection
@stop

