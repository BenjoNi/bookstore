@extends('app')

@section ('content')
    <h1>Edit: {!! ! $book->title !!}</h1>


 {!! Form::open(['method' => 'edit', 'action' =>'BooksController@edit', $book->book_id]) !!}
    <div class="form-group">
        {!! Form::label('title', 'Title of the book:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>


    <div class="form-group">
        {!! Form::label('author', 'Author:') !!}
        {!! Form::text('author', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('published', 'Published At:') !!}
        {!! Form::input('date','published',null,['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add Book', ['class' => 'btn btn-primary form-control']) !!}
    </div>
