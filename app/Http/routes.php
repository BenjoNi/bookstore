<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('books', 'BooksController@index');
Route::get('books/create', 'BooksController@create');
Route::post('books', 'BooksController@store');
Route::get('books/{id}/edit', 'BooksController@edit');
Route::get('books/{book_id}', 'BooksController@show');