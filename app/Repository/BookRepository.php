<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8.10.2015
 * Time: 12:16
 */

namespace App\Repository;
use App\contracts\IBookRepository;


use App\Book;

class BookRepository implements IBookRepository
{

    private $book;


    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function getAll()
    {
        return $this->book->all();
    }


    /**
     *
     */
    public function createBook()
    {
        $book = $this->book->create();
    }

    /**
     *
     */
    public function storeBook()
    {
        $book = $this->book->save();
    }


    /**
     *
     */
    public function show($book_id)
    {
        $book = $this->book->FindOrFail($book_id);
    }
};