<?php namespace App\Contracts;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.10.2015
 * Time: 10:18
 */

interface IBookRepository {
    public function getAll();
    public function createBook();
    public function storeBook();
    public function show($book_id);

}